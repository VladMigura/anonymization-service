package com.example.anonymizer;

import com.example.anonymizer.entity.ReplacementEntity;
import com.example.anonymizer.entity.ReplacementType;
import com.example.anonymizer.model.Message;
import com.example.anonymizer.repository.ReplacementRepository;
import com.example.anonymizer.service.AnonymizationService;
import com.example.anonymizer.service.impl.AnonymizationServiceImpl;
import com.example.anonymizer.util.ParsingUtil;
import com.github.javafaker.Faker;
import com.github.javafaker.Internet;
import com.github.javafaker.Number;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ApplicationTests {

    private final ReplacementRepository replacementRepositoryMock = mock(ReplacementRepository.class);
    private final Faker fakerMock = mock(Faker.class);
    private final Number numberMock = mock(Number.class);
    private final Internet internetMock = mock(Internet.class);

    private final AnonymizationService anonymizationService
            = new AnonymizationServiceImpl(fakerMock, replacementRepositoryMock);

    @Test
    void testParsingUtilHappyPath() {
        // GIVEN
        var id = "87432";
        var email = "lior@staircase.ai";
        var url = "https://staircase.ai/index.html";
        var input = format("Hello my order number is %s and my email address is %s. Please, visit my website %s",
                id, email, url);
        // WHEN
        var privateDataMap = ParsingUtil.findAllPrivateData(input);
        // THEN
        assertThat(privateDataMap).isNotEmpty();
        assertThat(privateDataMap.size()).isEqualTo(3);
        assertThat(privateDataMap.get(ReplacementType.ID)).isNotNull();
        assertThat(privateDataMap.get(ReplacementType.ID).contains(id)).isTrue();
        assertThat(privateDataMap.get(ReplacementType.EMAIL)).isNotNull();
        assertThat(privateDataMap.get(ReplacementType.EMAIL).contains(email)).isTrue();
        assertThat(privateDataMap.get(ReplacementType.URL)).isNotNull();
        assertThat(privateDataMap.get(ReplacementType.URL).contains(url)).isTrue();
    }

    @Test
    void testAnonymizationServiceHappyPath_idReplacement() {
        // GIVEN
        var id = "87432";
        var replacedId = "12345";
        var data = format("Hello my order number is %s", id);
        var message = new Message(data);
        var replacement = ReplacementEntity.builder()
                .id(UUID.randomUUID())
                .sourceData(id)
                .targetData(replacedId)
                .build();
        // WHEN
        when(fakerMock.number()).thenReturn(numberMock);
        when(numberMock.digits(anyInt())).thenReturn(replacedId);
        when(replacementRepositoryMock.findOneBySourceData(anyString())).thenReturn(Optional.empty());
        when(replacementRepositoryMock.saveAndFlush(any())).thenReturn(replacement);
        var anonymousMessage = anonymizationService.anonymize(message);
        var anonymousData = anonymousMessage.getData();
        // THEN
        assertThat(StringUtils.isNotEmpty(anonymousData)).isTrue();
        assertThat(anonymousData.contains(id)).isFalse();
        assertThat(anonymousData.contains(replacedId)).isTrue();
    }

    @Test
    void testAnonymizationServiceHappyPath_emailReplacement() {
        // GIVEN
        var email = "lior@staircase.ai";
        var replacedEmail = "example.user@example.com";
        var data = format("Hello my email is %s", email);
        var message = new Message(data);
        var replacement = ReplacementEntity.builder()
                .id(UUID.randomUUID())
                .sourceData(email)
                .targetData(replacedEmail)
                .build();
        // WHEN
        when(fakerMock.internet()).thenReturn(internetMock);
        when(internetMock.emailAddress()).thenReturn(replacedEmail);
        when(replacementRepositoryMock.findOneBySourceData(anyString())).thenReturn(Optional.empty());
        when(replacementRepositoryMock.saveAndFlush(any())).thenReturn(replacement);
        var anonymousMessage = anonymizationService.anonymize(message);
        var anonymousData = anonymousMessage.getData();
        // THEN
        assertThat(StringUtils.isNotEmpty(anonymousData)).isTrue();
        assertThat(anonymousData.contains(email)).isFalse();
        assertThat(anonymousData.contains(replacedEmail)).isTrue();
    }

    @Test
    void testAnonymizationServiceHappyPath_urlReplacement() {
        // GIVEN
        var urlTemplate = "https://%s/index.html";
        var url = format(urlTemplate, "staircase.ai");
        var fakeDomainName = "example.com";
        var replacedUrl = format(urlTemplate, fakeDomainName);
        var data = format("Hello my url is %s", url);
        var message = new Message(data);
        var replacement = ReplacementEntity.builder()
                .id(UUID.randomUUID())
                .sourceData(url)
                .targetData(replacedUrl)
                .build();
        // WHEN
        when(fakerMock.internet()).thenReturn(internetMock);
        when(internetMock.domainName()).thenReturn(fakeDomainName);
        when(replacementRepositoryMock.findOneBySourceData(anyString())).thenReturn(Optional.empty());
        when(replacementRepositoryMock.saveAndFlush(any())).thenReturn(replacement);
        var anonymousMessage = anonymizationService.anonymize(message);
        var anonymousData = anonymousMessage.getData();
        // THEN
        assertThat(StringUtils.isNotEmpty(anonymousData)).isTrue();
        assertThat(anonymousData.contains(url)).isFalse();
        assertThat(anonymousData.contains(replacedUrl)).isTrue();
    }

    @Test
    void testAnonymizationServiceHappyPath_idAlreadySaved() {
        // GIVEN
        var id = "87432";
        var replacedId = "12345";
        var data = format("Hello my order number is %s", id);
        var message = new Message(data);
        var replacement = ReplacementEntity.builder()
                .id(UUID.randomUUID())
                .sourceData(id)
                .targetData(replacedId)
                .build();
        // WHEN
        when(replacementRepositoryMock.findOneBySourceData(anyString())).thenReturn(Optional.of(replacement));
        var anonymousMessage = anonymizationService.anonymize(message);
        var anonymousData = anonymousMessage.getData();
        // THEN
        assertThat(StringUtils.isNotEmpty(anonymousData)).isTrue();
        assertThat(anonymousData.contains(id)).isFalse();
        assertThat(anonymousData.contains(replacedId)).isTrue();
    }

}
