package com.example.anonymizer.util;

import com.example.anonymizer.entity.ReplacementType;
import lombok.experimental.UtilityClass;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static com.example.anonymizer.entity.ReplacementType.*;
import static org.apache.commons.lang3.StringUtils.trim;

@UtilityClass
public class ParsingUtil {

    public static final String CASE_INSENSITIVE_PATTERN = "(?i)";
    public static final Pattern ID_PATTERN = Pattern.compile("(^\\d{3,})|(\\s\\d{3,}\\s)|(\\d{3,}$)");
    public static final Pattern EMAIL_PATTERN = Pattern.compile("((?:[a-z0-9!#$%&'*+/=?^_`\\{|\\}~-]+(?:\\."
                    + "[a-z0-9!#$%&'*+/=?^_`\\{|\\}~-]+)*|\\\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\"
                    + "x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\\\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                    + "?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25"
                    + "[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-"
                    + "\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\]))",
            Pattern.CASE_INSENSITIVE);
    public static final Pattern URL_PATTERN = Pattern.compile("(https?://(?:www\\.|(?!www))[a-zA-Z0-9]"
                    + "[a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}"
                    + "|https?://(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})",
            Pattern.CASE_INSENSITIVE);

    public static Map<ReplacementType, Set<String>> findAllPrivateData(final String input) {
        var idSet = findAllPrivateData(input, ID_PATTERN);
        var emailSet = findAllPrivateData(input, EMAIL_PATTERN);
        var urlSet = findAllPrivateData(input, URL_PATTERN);
        return Map.of(
                ID, idSet,
                EMAIL, emailSet,
                URL, urlSet
        );
    }

    public static Set<String> findAllPrivateData(final String input, final Pattern pattern) {
        var matcher = pattern.matcher(input);
        var resultSet = new HashSet<String>();
        while (matcher.find()) {
            resultSet.add(trim(matcher.group()));
        }
        return resultSet;
    }

}
