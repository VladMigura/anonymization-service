package com.example.anonymizer.util;

import lombok.experimental.UtilityClass;
import org.springframework.boot.SpringBootVersion;

import static org.apache.commons.lang3.StringUtils.firstNonEmpty;

@UtilityClass
public class VersionUtil {

    public static String appVersion() {
        return firstNonEmpty(System.getenv("APP_VERSION"), "unknown");
    }

    public static String springBootVersion() {
        return firstNonEmpty(SpringBootVersion.getVersion(), "unknown");
    }

    public static String javaVersion() {
        return firstNonEmpty(System.getProperty("java.runtime.version"),
                System.getProperty("java.vm.version"), "unknown");
    }

    public static String javaVendor() {
        return firstNonEmpty(System.getProperty("java.vendor"),
                System.getProperty("java.vm.vendor"), "unknown");
    }

}
