package com.example.anonymizer.api;

import com.example.anonymizer.model.Message;
import com.example.anonymizer.service.AnonymizationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
@RequestMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class AnonymizationApiController {

    private final AnonymizationService anonymizationService;

    @PostMapping(value = "/api/v1/anonymize")
    public ResponseEntity<Message> anonymize(final @Valid @RequestBody Message message) {
        var anonymousMessage = anonymizationService.anonymize(message);
        return new ResponseEntity<>(anonymousMessage, CREATED);
    }

}
