package com.example.anonymizer.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "replacement")
@EqualsAndHashCode(callSuper = false)
public class ReplacementEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false, unique = true)
    private UUID id;

    @Column(name = "source_data", updatable = false, unique = true)
    private String sourceData;

    @Column(name = "target_data", updatable = false)
    private String targetData;


}
