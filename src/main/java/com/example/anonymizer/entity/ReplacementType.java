package com.example.anonymizer.entity;

public enum ReplacementType {

    ID,
    EMAIL,
    URL

}
