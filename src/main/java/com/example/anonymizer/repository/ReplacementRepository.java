package com.example.anonymizer.repository;

import com.example.anonymizer.entity.ReplacementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ReplacementRepository extends JpaRepository<ReplacementEntity, UUID> {

    @Query(value = "SELECT * FROM replacement " +
            "WHERE source_data = :sourceData " +
            "AND deleted_at IS NULL ", nativeQuery = true)
    Optional<ReplacementEntity> findOneBySourceData(@Param("sourceData") String sourceData);

}
