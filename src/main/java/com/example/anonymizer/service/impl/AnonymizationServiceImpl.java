package com.example.anonymizer.service.impl;

import com.example.anonymizer.entity.ReplacementEntity;
import com.example.anonymizer.entity.ReplacementType;
import com.example.anonymizer.model.Message;
import com.example.anonymizer.repository.ReplacementRepository;
import com.example.anonymizer.service.AnonymizationService;
import com.example.anonymizer.util.ParsingUtil;
import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.example.anonymizer.util.ParsingUtil.CASE_INSENSITIVE_PATTERN;
import static org.apache.commons.lang3.StringUtils.lowerCase;

@Slf4j
@Service
@AllArgsConstructor
public class AnonymizationServiceImpl implements AnonymizationService {

    private final Faker faker;
    private final ReplacementRepository replacementRepository;

    @Override
    @Transactional
    public Message anonymize(final Message message) {
        var privateDataMap = ParsingUtil.findAllPrivateData(message.getData());
        var replacements = privateDataMap.entrySet().stream()
                .flatMap(this::getOrCreateReplacements)
                .collect(Collectors.toList());
        var anonymousData = replaceAllData(message.getData(), replacements);
        return new Message(anonymousData);
    }

    private Stream<ReplacementEntity> getOrCreateReplacements(final Map.Entry<ReplacementType, Set<String>> dataEntry) {
        var replacementType = dataEntry.getKey();
        return dataEntry.getValue().stream()
                .map(sourceData -> replacementRepository.findOneBySourceData(lowerCase(sourceData))
                        .or(() -> createReplacement(sourceData, replacementType))
                        .orElseThrow(RuntimeException::new));
    }

    private Optional<ReplacementEntity> createReplacement(final String sourceData, final ReplacementType type) {
        var entity = ReplacementEntity.builder()
                .sourceData(lowerCase(sourceData))
                .targetData(lowerCase(generateTargetData(sourceData, type)))
                .build();
        return Optional.of(replacementRepository.saveAndFlush(entity));
    }

    private String generateTargetData(final String sourceData, final ReplacementType type) {
        switch (type) {
            case ID:
                return faker.number().digits(5);
            case EMAIL:
                return faker.internet().emailAddress();
            case URL:
                return UriComponentsBuilder.fromUriString(sourceData)
                        .host(faker.internet().domainName())
                        .toUriString();
            default:
                log.debug("Unknown replacement type!");
                return "";
        }
    }

    private String replaceAllData(final String input, final List<ReplacementEntity> replacements) {
        var result = input;
        for (var replacement : replacements) {
            result = result.replaceAll(
                    CASE_INSENSITIVE_PATTERN + Pattern.quote(replacement.getSourceData()),
                    replacement.getTargetData());
        }
        return result;
    }

}
