package com.example.anonymizer.service;

import com.example.anonymizer.model.Message;

public interface AnonymizationService {

    Message anonymize(Message message);

}
