CREATE TABLE replacement
(
    id          UUID        DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT replacement_pkey PRIMARY KEY,
    source_data TEXT                                   NOT NULL
        CONSTRAINT replacement_source_data_unique UNIQUE,
    target_data TEXT                                   NOT NULL,
    created_at  TIMESTAMPTZ DEFAULT now()              NOT NULL,
    updated_at  TIMESTAMPTZ,
    deleted_at  TIMESTAMPTZ
);