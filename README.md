# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.3/gradle-plugin/reference/html/)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.5.3/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.3/reference/htmlsingle/#boot-features-developing-web-applications)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/2.5.3/reference/htmlsingle/#howto-execute-flyway-database-migrations-on-startup)

### Guides

The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

### Prerequisites
Project requires Java 11, Docker, PostgreSQL(optional if you launch database via docker).

### Running
Project requires running PostgreSQL database. You can run it via `docker-compose up -d` or by adding it as 
datasource to IntelliJ Idea. To access server endpoints please see the project's OpenAPI documentation 
(http://localhost:8080). 

### Running the tests
`./gradlew test` for MacOS or Linux and `gradlew test` for Windows.
